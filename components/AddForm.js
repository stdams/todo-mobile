import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import Todos from './Todos'


export default class AddForm extends Component {
    state = {
        content: ''
    }
    handleChange = (text) => {
        
        this.setState({
            content: text
        })
        
    }
    handleSumbmit = (e) => {
        if (this.state.content.length) {
            this.props.addTodo(this.state)
            this.setState({
                content: ''
            })
        }
    }
    render () {
        return (
            <View>
                <View style={styles.footer}>
                <TextInput style={styles.textInput} placholder='>note' placeholderTextColor='white'  onChangeText={this.handleChange} value={this.state.content}/>
                </View>
                <TouchableOpacity style={styles.addButton} onPress={this.handleSumbmit}>
                <Text style={styles.addButtonText} >+</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  
  footer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      zIndex: 10,
  },
  textInput: {
      alignSelf: 'stretch',
      color: '#fff',
      padding: 20,
      backgroundColor: '#252525',
      borderTopWidth: 0,
      borderTopColor: '#ededed',
  },
  addButton: {
      position: 'absolute',
      zIndex: 11,
      right: 20,
      bottom: 90,
      backgroundColor: '#e91e43',
      width: 90,
      height: 90,
      borderRadius: 30,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 8,
  },
  addButtonText: {
      color: '#fff',
      fontSize: 24,
  },
});
