import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';

const Todos = ({todos, deleteTodo}) => {

    const todoList = todos && todos.length ? (
        todos.map(todo => {
            return (
                <View style={styles.note} key={todo.id}>  
                    <TouchableOpacity onPress={()=> {deleteTodo(todo.id)}}>
                        <Text>{todo.content}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
    ): (
         <Text > You have not todos left, yay! </Text>
    )
    return (
        <View>{todoList}</View>
    )
}


export default Todos;


const styles = StyleSheet.create({
    note: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
    },
})