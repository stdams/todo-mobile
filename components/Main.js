import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import Todos from './Todos'
import AddForm from './AddForm'
import {connect} from 'react-redux';
import {deleteTodo, addTodo} from '../actions/todoActions'
import {firestoreConnect} from 'react-redux-firebase'
import { compose} from "redux";
import {decode, encode} from 'base-64'
if (!global.btoa) {  global.btoa = encode }
if (!global.atob) { global.atob = decode }



class Main extends Component {
  

  deleteTodo = (id) => {
      this.props.deleteTodo(id)
      //const todos = this.props.todos.todos.filter(todo => {
      //    return todo.id !== id
      //});
      //this.setState({
      //    todos
      //})
  }
  addTodo = (todo) => {
      this.props.addTodo(todo)
    //todo.id = Math.random();
    //let todos = [...this.props.todos, todo];
    //this.setState({
    //    todos
    //})
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
            <Text style={styles.headerText}>MY TODO APP</Text>
        </View>
        <ScrollView style={styles.scrollContainer}>
            <Todos todos={this.props.todos} deleteTodo={this.deleteTodo} />
        </ScrollView>
        <AddForm addTodo={this.addTodo}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
    //console.warn(state)
    return {
        
        todos: state.firestore.ordered.todos,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteTodo: (id) => {dispatch(deleteTodo(id))},
        addTodo: (todo) => {dispatch(addTodo(todo))}
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        {collection: 'todos'}
    ])
) (Main);










const styles = StyleSheet.create({
  container: {
      flex: 1
  },
  header: {
      backgroundColor: '#e91e63',
      alignItems: 'center',
      justifyContent: 'center',
      borderBottomWidth: 10,
      borderBottomColor: '#ddd'
  },
  headerText: {
      color: 'white',
      fontSize: 18,
      padding: 26,
  },
  scrollContainer: {
      flex: 1,
      marginBottom: 50,
  },
  footer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      zIndex: 10,
  },
  textInput: {
      alignSelf: 'stretch',
      color: '#fff',
      padding: 20,
      backgroundColor: '#252525',
      borderTopWidth: 0,
      borderTopColor: '#ededed',
  },
  addButton: {
      position: 'absolute',
      zIndex: 11,
      right: 20,
      bottom: 90,
      backgroundColor: '#e91e43',
      width: 90,
      height: 90,
      borderRadius: 30,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 8,
  },
  addButtonText: {
      color: '#fff',
      fontSize: 24,
  },
});
