import {createStore, combineReducers} from 'redux';
import rootReducer from '../reducer/rootReducer'
import thunk from 'redux-thunk'
import {applyMiddleware, compose} from 'redux'
import {reduxFirestore, getFirestore, firestoreReducer} from 'redux-firestore'
import {reactReduxFirebase, getFirebase} from 'react-redux-firebase'
import fbConfig from '../config/fbConfig'

const myReducer = combineReducers({
    todos: rootReducer,
    firestore: firestoreReducer
});

const configureStore = () => {
    return createStore(myReducer, 
        compose(
            applyMiddleware(thunk.withExtraArgument({getFirebase, getFirestore})),
            reduxFirestore(fbConfig),
            reactReduxFirebase(fbConfig)
        )
    )
}

export default configureStore
