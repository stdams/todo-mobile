const initState = {
    todos: [
        {id: '1', content: 'buy some milk'},
        {id: '2', content: 'play mario kart'}
    ]
}

const rootReducer = (state= initState, action) => {
    if (action.type === 'DELETE_TODO') {
        //console.warn('todo deleted', action.id)
        return state
        //let newTodos= state.todos.filter(todo => {
        //    return todo.id !== action.id
        //})
        //return {
        //    ...state,
        //    todos: newTodos
        //}
    } else
    if (action.type === 'DELETE_TODO_ERR') {
        console.warn('deleting err', action.err)
    }
    else 
    if (action.type === 'ADD_TODO') {
        //console.warn('created todo', action.todo)
        return state
        //const todo = action.todo;
        //todo.id = Math.random()
        //let newTodos= [...state.todos, todo];
        //    return {
        //        ...state,
        //        todos: newTodos
        //    }
    } else
    if (action.type === 'ADD_TODO_ERR') {
        console.warn('add todo error', action.err)
        return state
    }
    return state;
}

export default rootReducer